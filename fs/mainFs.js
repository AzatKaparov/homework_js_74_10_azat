const fs = require('fs');

const path = './messages';
let data = [];

module.exports = {
    getItems() {
        data = [];
        const newData = fs.readdirSync(`${path}/`);
        newData.forEach(file => {
            const fileContent = fs.readFileSync(`${path}/${file}`)
            data.push(JSON.parse(fileContent));
        })
        return data.slice(data.length -5, data.length);
    },
    addItem: function (item, date) {
        data = item;
        try {
            const fileContents = fs.readFileSync(`${path}/${date}.json`);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = item;
        }
        this.save(date);
    },
    save(date) {
        fs.writeFileSync(`${path}/${date}.json`, JSON.stringify(data));
    }
};