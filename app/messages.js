const express = require('express');
const fileDb = require('../fs/mainFs');
const router = express.Router();


router.get('/', (req, res) => {
    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/create', (req, res) => {
    const dateObj = Date.now();
    const product = {...req.body, datetime: dateObj};
    fileDb.addItem(product, product.datetime);
    res.send(product);
});

module.exports = router;